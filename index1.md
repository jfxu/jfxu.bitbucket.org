---
title       : Slidify and rCharts
subtitle    : DVDC and SPDC Meetup
---

--- .nobackground .quote

<q> Slidify helps __create__, customize and share, elegant, dynamic and interactive HTML5 documents using R Markdown.</q>

---

<iframe src='assets/img/create_deck.svg' width=960px></iframe>

--- .segue bg:blue

## Demo 1 | Create

--- .nobackground .quote

<q> Slidify helps create, __customize__ and share, elegant, dynamic and interactive HTML5 documents using R Markdown.</q>

---

## Customization

Slidify is highly modular and attempts to achieve clean separation of content from view and behavior. There are several ways to customize a document.

> 1. Frameworks
> 2. Layouts
> 3. Widgets

--- .segue bg:blue

## Demo 2 | Frameworks

--- .segue bg:indigo

## How Slidify Works?

---

<iframe src='assets/img/knit.svg' width=800px height=250px>
</iframe> 

---

<iframe src='assets/img/split_apply_combine.svg' width=800px height=250px>
</iframe> 


---

<iframe src='assets/img/split.svg' width=800px height=250px>
</iframe> 

---

<iframe src='assets/img/apply.svg' width=800px height=250px>
</iframe> 


---

<iframe src='assets/img/combine.svg' width=800px height=250px>
</iframe> 

--- .segue bg:indigo

## Journey of a Slide

--- .bigger

## Slide



    --- {class: class1, bg: yellow, id: id1}
    
    ## Slide Title
    
    Slide Contents
    
    *** =pnotes
    
    Some notes

---

<iframe src='assets/img/raw_slide.svg' width=800px height=250px>
</iframe> 

---

<a class='example'>Payload</a>


```
$html
[1] "<h2>Slide Title</h2>\n\n<p>Slide Contents</p>\n"

$header
[1] "<h2>Slide Title</h2>"

$level
[1] "2"

$title
[1] "Slide Title"

$content
[1] "<p>Slide Contents</p>\n"

$class
[1] "class1"

$bg
[1] "yellow"

$id
[1] "id1"

$pnotes
$pnotes$html
[1] "<p>Some notes</p>\n"

$pnotes$level
[1] NA

$pnotes$title
[1] NA

$pnotes$content
[1] "<p>Some notes</p>\n"

$pnotes$name
[1] "pnotes"


$blocks
list()
```

---

<iframe src='assets/img/parse_slide.svg' width=800px height=250px>
</iframe> 


--- .RAW .bigger

<a class='example'>Layout</a>


```
<slide class="{{ slide.class }}" id="{{ slide.id }}" style="background:{{{ slide.bg }}};">
  {{# slide.header }}
  <hgroup>
    {{{ slide.header}}}
  </hgroup>
  {{/ slide.header }}
  <article data-timings="{{ slide.dt }}">
    {{{ slide.content }}}
  </article>
  <!-- Presenter Notes -->
  {{# slide.pnotes }}
  <aside class="note" id="{{ id }}">
    <section>
      {{{ html }}}
    </section>
  </aside>
  {{/ slide.pnotes }}
</slide>
```

---

<iframe src='assets/img/layout_slide.svg' width=800px height=250px>
</iframe> 

--- .bigger

<a class='example'>Rendered</a>


```
Error in loadNamespace(name): there is no package called 'rCharts'
```

```
Error in cat(rendered): object 'rendered' not found
```

---

<iframe src='assets/img/render_slide.svg' width=800px height=250px>
</iframe> 

--- {class: class1, bg: yellow, id: id1}
    
## Slide Title
    
Slide Contents
    
*** =pnotes
    
Some notes

--- .segue .nobackground .dark

## Slide Properties

---

<img style='margin-top: -40px' class="center" src='assets/img/slide_properties.svg' width=960px height=250px></img> 

---

## Slide Properties

| **Variable**    | **Description**                       |
|-----------------|---------------------------------------|
| `slide.title`   | The title of the slide with no markup |
| `slide.header`  | The title of the slide with markup    |
| `slide.level`   | The title header level (h1 - h6)      |
| `slide.content` | The contents of the slide sans header |
| `slide.html`    | The contents of the slide with header |
| `slide.num`     | The number of the slide               |
| `slide.id`      | The id assigned to the slide          |
| `slide.class`   | The class assigned to the slide       |
| `slide.bg`      | The background assigned to the slide  |
| `slide.myblock`   | The slide block named myblock       |
| `slide.blocks`  | The slide blocks which are not named  |
| `slide.rendered`| The rendered slide                    |

--- .segue .nobackground .dark

## Slide Properties

---

## Carousel Slide

<a class='example'>slide</a>

    --- &carousel .span12
    
    ## Carousel
    
    
    *** {class: active, img: "http://placehold.it/960x500"}
    
    Image 1
    
    *** {img: "http://placehold.it/960x500"}
    
    Image 2



--- .RAW .smaller

<a class='example'>layout</a>


```
## Warning in file(con, "r"): cannot open file 'assets/layouts/carousel.html':
## No such file or directory
```

```
## Error in file(con, "r"): cannot open the connection
```


--- &carousel .span12

## Carousel

<a class='example'>view</a>


*** {class: active, img: "http://placehold.it/960x500"}

Image 1

*** {img: "http://placehold.it/960x500"}

Image 2

--- .nobackground .quote

<q> Slidify helps create, customize and __share__, elegant, dynamic and interactive HTML5 documents using R Markdown.</q>

--- .bigger

## Publish

Share your document easily on [github](http://github.com), [rpubs](http://rpubs.com) and [dropbox](http://dropbox.com)

```
slidify('index.Rmd')
publish('mydeck', 'ramnathv')
```

--- .nobackground .quote

<q> Slidify helps create, customize and share, elegant, __dynamic__ and interactive HTML5 documents using R Markdown.</q>

---

## Dynamic



---

## HT to knitr and @yihui

<img class='center' src='http://ecx.images-amazon.com/images/I/41kI1dxXGfL.jpg' />

--- .nobackground .quote

<q> Slidify helps create, customize and share, elegant, dynamic and __interactive__ HTML5 documents using R Markdown.</q>

---

## Interactive Quiz

    --- &radio  
    ## Interactive Quiz
    
    What is 1 + 1?
    
    1. 1 
    2. _2_
    3. 3
    4. 4
    
    *** .hint
    
    This is a hint
    
    *** .explanation
    
    This is an explanation

--- &radio

## Interactive Quiz

What is 1 + 1?

1. 1 
2. _2_
3. 3
4. 4

*** .hint

This is a hint

*** .explanation

This is an explanation

--- .bigger

## Interactive Chart


```r
require(rCharts)
```

```
## Warning in library(package, lib.loc = lib.loc, character.only = TRUE,
## logical.return = TRUE, : there is no package called 'rCharts'
```

```r
haireye = as.data.frame(HairEyeColor)
n1 <- nPlot(Freq ~ Hair, 
  group = 'Eye',
  data = subset(haireye, Sex == 'Male'),
  type = 'multiBarChart'
)
```

```
## Error in eval(expr, envir, enclos): could not find function "nPlot"
```

```r
n1$print('chart1')
```

```
## Error in eval(expr, envir, enclos): object 'n1' not found
```

---

## Interactive Chart


```
## Loading required package: rCharts
```

```
## Warning in library(package, lib.loc = lib.loc, character.only = TRUE,
## logical.return = TRUE, : there is no package called 'rCharts'
```

```
## Error in eval(expr, envir, enclos): could not find function "nPlot"
```

```
## Error in eval(expr, envir, enclos): object 'n1' not found
```

--- .segue bg:blue

## Interactive Slides Demo

--- .segue .dark

## Interactive Visualizations

--- .fill .nobackground

![img](assets/img/rCharts.png)

--- .quote

<q>rCharts is an R package to create, customize and share interactive visualizations, using a lattice-like formula interface.</q>

--- .segue .dark .nobackground

## rCharts Demos

---

## Basic Plot


```r
r1 <- rPlot(mpg ~ wt | am + vs, 
  data = mtcars, 
  color = 'gear',
  type = 'point'
)
r1
```

---


```
## Error in eval(expr, envir, enclos): could not find function "rPlot"
```

```
## Error in eval(expr, envir, enclos): object 'r1' not found
```

---

## Add Controls


```r
r1$addControls("x", "wt", names(mtcars))
r1$addControls("y", "mpg", names(mtcars))
r1
```

---


```
## Error in eval(expr, envir, enclos): object 'r1' not found
```

```
## Error in eval(expr, envir, enclos): object 'r1' not found
```

```
## Error in eval(expr, envir, enclos): object 'r1' not found
```

---

## Publish Chart



---

## Credits

1. Kenton Russel and Thomas Reinholdsson for coauthoring rCharts.
2. Yihui Xie for knitr.
3. Joe Cheng for Shiny.
4. Jeffrey Horner and RStudio for Markdown.
5. Hadley Wickham for several R packages.
6. Authors of all the JS Libraries I have liberally used.
7. Authors of several presentation libraries in Ruby/Python/JS.










