---
title       : First template with slidify
subtitle    : testing the subtitle
author      : Jianfeng Xu
job         : 
framework   : io2012      # {io2012, html5slides, shower, dzslides, ...}
highlighter : highlight.js  # {highlight.js, prettify, highlight}
hitheme     : tomorrow      # 
widgets     : [mathjax]            # {mathjax, quiz, bootstrap}
mode        : selfcontained # {standalone, draft}
logo        : logo.jpg
knit        : slidify::knit2slides
---

# Data Visualisation in R

[Jianfeng Xu](ttp://www.ventana.com)

23-FEB-2016

---

## Agenda

* R Graphics: Why R? [[[]]]=frag-highlight-blue
* Traditional Graphics [[[]]]=frag-highlight-blue
* ggplot2 [[[]]]=frag-highlight-blue
* Devices in  R [[[]]]=frag-highlight-blue

---

## Where is the title

> * Edit YAML front matter

> * Write using R Markdown

> * Use an empty line followed by three dashes to separate slides!

--- .class #id 


## Quick example with ggplot2

```{r, echo=6}
require(ggplot2)
data(diamonds)
diamonds<-diamonds[sample(1:nrow(diamonds),2000),]
ggplot(diamonds, aes(carat, price))+geom_point(color="firebrick")
```

---

## Example 1 : googleVis
[WorldBank data](http://viz.vinux.in/datavis/pic/WorldBankMotionChart.html)
<iframe height="450px" frameborder="no" scrolling="yes"
src="viz.vinux.in/datavis/pic/WorldBankMotionChart.html"
width="100%">
</iframe>
```{r eval=FALSE, echo = TRUE}
library(googleVis)
demo(WorldBank)
```

---

## Example 3: Plot using map
[[[From my previous talk: R Graphics with ggplot2]]]=small

```{r, message=FALSE, warning=FALSE, echo=TRUE, fig.width=5, fig.height=5, fig.cap=""}
require(ggplot2)
require(plyr)
require(maptools)
require(rgdal)
require(RColorBrewer)
require(gpclib)
a <- 1:10
a^3
```

---

# Thank You