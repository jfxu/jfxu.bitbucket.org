---
title       : First template with slidify
subtitle    : testing the subtitle
author      : Jianfeng Xu
job         : 
framework   : io2012      # {io2012, html5slides, shower, dzslides, ...}
highlighter : highlight.js  # {highlight.js, prettify, highlight}
hitheme     : tomorrow      # 
widgets     : [mathjax]            # {mathjax, quiz, bootstrap}
mode        : selfcontained # {standalone, draft}
logo        : logo.jpg
knit        : slidify::knit2slides
---

# Data Visualisation in R

[Jianfeng Xu](ttp://www.ventana.com)

23-FEB-2016

---

## Agenda

* R Graphics: Why R? [[[]]]=frag-highlight-blue
* Traditional Graphics [[[]]]=frag-highlight-blue
* ggplot2 [[[]]]=frag-highlight-blue
* Devices in  R [[[]]]=frag-highlight-blue

---

## Where is the title

> * Edit YAML front matter

> * Write using R Markdown

> * Use an empty line followed by three dashes to separate slides!

--- .class #id 


## Quick example with ggplot2

![plot of chunk unnamed-chunk-1](assets/fig/unnamed-chunk-1-1.png)

---

## Example 1 : googleVis
[WorldBank data](http://viz.vinux.in/datavis/pic/WorldBankMotionChart.html)
<iframe height="450px" frameborder="no" scrolling="yes"
src="viz.vinux.in/datavis/pic/WorldBankMotionChart.html"
width="100%">
</iframe>

```r
library(googleVis)
demo(WorldBank)
```

---

## Example 3: Plot using map
[[[From my previous talk: R Graphics with ggplot2]]]=small


```r
require(ggplot2)
require(plyr)
require(maptools)
require(rgdal)
require(RColorBrewer)
require(gpclib)
a <- 1:10
a^3
```

```
##  [1]    1    8   27   64  125  216  343  512  729 1000
```

---

# Thank You
